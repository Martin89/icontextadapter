package de.martingolpashin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Martin on 23.07.14.
 */
public class IconTextAdapter extends BaseAdapter {


    private ArrayList<NavigationListItem> items;

    private Activity activity;
    private static LayoutInflater inflater;

    public IconTextAdapter(Activity activity, ArrayList<NavigationListItem> items) {
        this.items = items;
        this.activity = activity;
        inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class Viewholder{
        public ImageView icon;
        public TextView text;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View view = convertView;
        Viewholder viewholder;

        if(convertView == null){
            view = inflater.inflate(R.layout.item_icon_text,null);

            viewholder = new Viewholder();

            viewholder.icon = (ImageView) view.findViewById(R.id.imageView);
            viewholder.text = (TextView) view.findViewById(R.id.textView);

            view.setTag(viewholder);
        }else{
            viewholder = (Viewholder)convertView.getTag();
        }

        viewholder.icon.setImageResource(items.get(i).getImageResourceId());
        viewholder.text.setText(items.get(i).getName());

        return view;
    }
}
