package de.martingolpashin.android.adapter;

import android.app.Fragment;

/**
 * Created by Martin on 23.07.14.
 */
public class NavigationListItem {
    private int imageResourceId;
    private String name;
    private Fragment fragment;

    public NavigationListItem(int imageResourceId, String name, Fragment fragment) {
        this.imageResourceId = imageResourceId;
        this.name = name;
        this.fragment = fragment;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public String getName() {
        return name;
    }

    public Fragment getFragment() {
        return fragment;
    }
}
